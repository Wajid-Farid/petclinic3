#!/bin/bash

# RHEL systems
# chkconfig: 345 90 90
# description: PetClinic application

# Debian Systems
### BEGIN INIT INFO
# Provides:          PetClinic
# Required-Start:    $local_fs $network $named $time $syslog
# Required-Stop:     $local_fs $network $named $time $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description:       The PetClinic application
### END INIT INFO

case $1 in
  start)  # Run your script here
          cd /home/ubuntu/spring-petclinic
          sudo ./mvnw spring-boot:run
     ;;
esac